package lab2;

import cse131.ArgsProcessor;

public class RockPaperScissors {

	public static void main(String[] args) {
			ArgsProcessor ap = new ArgsProcessor(args);
			int player2Wins = 1;
			int ties = 2;

			int N=ap.nextInt("How many games of Rock Paper Scissors should they play?");
			String player1 = "rock";
			for (int i=0; i<N ; i++){	
				if (player1 == "rock"){
					player1 = "paper";
				}
				else if (player1 == "paper")	{
					player1 =  "scissors";
				}
				else if (player1 == "scissors") {
					player1 = "rock";	
				}

				double random = Math.random();
				//if (counter == 0) {
				String player2 = "rock";

				if (random < .33)   {
					player2 = "scissors";
				}
				// make the player rock;                   
				//increase the counter (counter = 1);                    
				else if (random >= .33 && random < .67) {
					player2 = "paper";
				}
				// make the player paper;                     

				// increase the counter (counter = 2);              
				else if (random >= .67 ) {
					player2 = "rock";     
				}	
				if	(player1 == "rock" && player2 == "rock" ){
					System.out.println("Tied Game");
				}			
				if (player1 == "rock" && player2 == "paper"){
					System.out.println("Random wins");
					player2Wins++;
				}
				if (player1 == "rock" && player2 == "scissors"){
					System.out.println("Geek wins");
				}
				if	(player1 == "scissors" && player2 == "scissors" ){
					System.out.println("Tied Game");
				}
				if (player1 == "scissors" && player2 == "paper"){
					System.out.println("Geek wins");
				}
				if	(player1 == "scissors" && player2 == "rock" ){
					System.out.println("Random wins");
					player2Wins++;
				}
				if	(player1 == "paper" && player2 == "paper" ){
					System.out.println("Tied Game");

					ties++;
					if	(player1 == "paper" && player2 == "rock" ){
						System.out.println("Geek wins");
					}
					if	(player1 == "paper" && player2 == "scissors" ){
						System.out.println("Random wins");
						player2Wins++;

			}
				}
		}
		System.out.println("Geek threw " + player1);
		System.out.println("Random threw " + player2);
		//System.out.println(1.0 * player1Wins/N);
		//System.out.println(1.0 * player2Wins/N);

		System.out.println("Random player wins "+player2Wins+" times in "+N+" number of games");
		// random players wins player2Win / n

		System.out.println("Fraction of Random  wins is : "   + (double)player2Wins/N);
		System.out.println("Fraction of tied games is : "   + (double)ties/N);
		System.out.println("Fraction of Geek wins is : "  + (double)(N-(ties +player2Wins))/N);
	
	

	}

}
