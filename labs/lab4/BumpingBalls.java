package lab4;

import sedgewick.StdDraw;
import cse131.ArgsProcessor;

public class BumpingBalls {

	public static void main(String[] args) {

		ArgsProcessor ap = new ArgsProcessor(args);
		//request from user number of balls
		int numBalls = ap.nextInt("How many balls will you put into play?");

		// set the scale of the coordinate system
		StdDraw.setXscale(-1.0, 1.0);
		StdDraw.setYscale(-1.0, 1.0);

		// initial values
		double[] positionX = new double[numBalls];
		double[] positionY = new double[numBalls];
		double[] velocityX = new double[numBalls];
		double[] velocityY = new double[numBalls];
		double[] radius = new double[numBalls];

		for (int i = 0; i < numBalls; ++i) {
			positionX[i] = Math.random();
			positionY[i] = Math.random();
			velocityX[i] = Math.random() * .01;
			velocityY[i] = Math.random() * .01;
		}

		while (true){
			// clear the background - draw before since it is on the bottom
			StdDraw.setPenColor(StdDraw.CYAN);

			int i = 0;
			positionX[i] = positionX[i] + velocityX[i]; 
			positionY[i] = positionY[i] + velocityY[i];
			positionX[i] = positionX[i] + velocityX[i];

			//figure out the distance between, if balliradius + balliradius >= distance between, then reverse direction
			for (int j = 0; j < numBalls; ++j){
				//create a for loop
			}
			double distance = Math.sqrt(Math.pow(positionX[i] - positionX[i], 2) + Math.pow(positionY[i] - positionY[i], 2)); 
			int j = 0;
		
			if (distance <= radius[i] + radius[j]){ 
				//if distance between two balls enables touching/overlapping, change direction
				// and undo last velocity update; now the balls aren't overlapping anymore
				positionX[i] = positionX[i] - velocityX[i] ;
				positionY[i] = positionY[i] - velocityY[i] ;
				positionX[j] = positionX[j] - velocityX[j] ;
				positionY[j] = positionY[j] - velocityY[j] ;
				
				// now reverse all velocity components
				velocityX[i] = -velocityX[i] ;
				velocityY[i] = -velocityY[i] ;
				velocityX[j] = -velocityX[j] ;
				velocityY[j] = -velocityY[j] ;

				//if balliradius + balliradius < distance between, then keep moving
				positionX[i] = positionX[i] + velocityX[i];
				positionY[i] = positionY[i] + velocityY[i];

			} 
			
			// clear the background
            StdDraw.clear(StdDraw.GRAY);

            // draw ball on the screen
            StdDraw.setPenColor(StdDraw.BLACK); 
        	
            StdDraw.show();

            // pause for 20 ms
			StdDraw.pause(20);
		}
	}
}



