package lab5;

public class Lab5Methods {

	// sum of the positive integers n + (n-2) + (n-4) + …*/
	public static void main(String[] args) {

		int s = sumDownBy2(7);
		System.out.println(s);
		int p = multPos(3,7);
		System.out.println(p);
		int mn = mult(-3,-4);
		int mz = mult(0,3);
		int mp = mult(3,4);
		int mm = mult(-3,4);
		System.out.println("mn = "+mn+" mz = "+mz+" mp = "+mp+" mm = "+mm);
	}
	public static int sumDownBy2(int n){ 
		int sum=0;
		if(n <= 0){
			return 0;
		} else {
			for(int j=n;j>=0;j-=2)
			{sum = sum +j;}
		}
		return sum;
	}//sumdown function closes

	/*7TH Problem :Write and test a method multPos(int i, int j) with the following specification.
					PARAMETERS: positive integers j and k RETURN VALUE: the product j*k
					Without using the multiplication operator. Use iteration and repeated addition to form the product.*/

	public static int multPos(int j, int k){
		if(j<=0 || k <=0)
		{
			System.out.println("Enter a positive number only");
		}
		int prod = 0;
		for(int i = 1; i <= k; i++)
		{
			prod = prod + j;
		}
		return prod;
	}

	/* 8th Problem Method mult that takes in integers j & k and returns their product.
	 *  Each integer could be positive,negative,or zero.
	 *  Do this by calling the multPos method,passing absolute values of j and k. It should return the product of those positive values.
		        Compute this method’s return value based on the result of multPos and based on the signs of j and k.*/

	public static int mult(int j,int k){
		int prod;
		if ((j <= 0 && k <=0) || (j >= 0 && k >=0)){
			prod = multPos(Math.abs(j), Math.abs(k));
		}
		else
		{
			prod = -multPos(Math.abs(j), Math.abs(k));
		}

		return prod;
	}
}