package studio2;

import cse131.ArgsProcessor;

public class GamblersRuin {

	public static void main(String[] args) {

		ArgsProcessor ap = new ArgsProcessor(args);
		double startAmount = ap.nextDouble("The amount of money you would like to use to gamble?");
		double winChance = ap.nextDouble("The probability that you win a gamble");
		double  winAmount = ap.nextDouble("The amount of money you want to have when you walk away");
		double totalPlays = ap.nextInt("The number of times you simulate the problem");
		int k = 0;
		double z= Math.random();
		double Ruin = 0;
		for( int i = 0; i <= totalPlays ; i ++)
		{
			System.out.print("Simulation " + i + ":");
			double j = startAmount;
			int round = 0 ;
			while(j>0 && j <winAmount)
			{
				round++;
				if (winChance>z)
					j++;
				else
					j--;
			}
			System.out.print(round + " rounds " );
			if(j == winAmount)
			{
				System.out.print("win \n" );
			}
			else
			{
				System.out.print("lose \n" );
				k++;
			}
		}
		System.out.println("Actual Ruin Rate: ");
		double actualRuinRate = k / totalPlays*1.0;
		System.out.println(actualRuinRate);
		if (z != winChance) 
		{
			Ruin = Math.pow((z/winChance),startAmount) -Math.pow( (z/winChance),winAmount) / (1 - Math.pow(z/winChance,winAmount)); }
		if (z == winChance)
		{
			Ruin = 1 - (startAmount / winAmount);}
		System.out.print(Ruin);
	}
}
//public class Gambler { 

