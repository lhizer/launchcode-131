package studio2;

import cse131.ArgsProcessor;

public class pi {

	public static void main(String[] args) {

		double ans = 0.0;
		ArgsProcessor ap = new ArgsProcessor (args);
		//ask how many darts they want to throw

		int darts = ap.nextInt("How many darts do you want to throw?");
		int insideDarts = 0;
		for(int i = 0 ; i < darts; i++){
			// we know that x1 and y1 are .5 and > .5 is outside of the circle
			double x2 = Math.random();
			double y2 = Math.random();
			double distance = Math.sqrt(Math.pow(.5 - x2, 2) + Math.pow(.5 - y2, 2));

			if (distance <= .5){
				insideDarts++;
			}
		}
		//  fill in to compute ans = Pi
		ans = (insideDarts / (darts * .25));

		System.out.println("Our group shows Pi = " + ans);








	}
}
